describe user('elasticsearch') do
  it { should exist }
end

describe group('elasticsearch') do
  it { should exist }
end

describe file('/etc/elasticsearch') do
 its('type') { should eq :directory }
 it { should be_directory }
end

describe file('/etc/elasticsearch/elasticsearch.yml') do
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
end

describe service('elasticsearch') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end
